![alt text](<https://gitlab.com/fet64/scripts/-/raw/main/screen.png>)

# scripts

Some scripts

- Copy bspwmrc to $HOME/.config/bspwm/
- Copy and rename polybar_config to $HOME/.config/polybar/config
- Copy sxhkdrc to $HOME/.config/sxhkd/
- Copy m_edit_config to /usr/local/bin/m_edit_config
- Copy m_kill.sh to /usr/local/bin/m_kill
- Copy m_man.sh to /usr/local/bin/m_man
- Copy m_help to /usr/local/bin/m_help


## What I use

- Window Manager: https://github.com/baskerville/bspwm
- Hotkey daemon:  https://github.com/baskerville/sxhkd
- Status bar:     https://github.com/polybar/polybar

